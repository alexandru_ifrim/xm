$(function () {
    var today = new Date();
    var startDate = new Date(today.getFullYear(), 11, 18);
    var endDate = new Date(today.getFullYear(), 11, 20);
    var $body = $("body");

    if (today >= startDate && today < endDate) {
        if (window.location.pathname.indexOf("Main") >= 0) {
            var $lma = $("<div/>");
            $lma.addClass("lma");
            $lma.html("<div class=\"tree\"></div><div class=\"santa\"></div><h1>Happy Birthday</h1><h1>Alexa!</h1><h2>&mdash; ebs team</h2>");

            setTimeout(function () {
                $("#wrapper").css({ opacity: 1 }).animate({ opacity: 0 }, 1000, function () {
                    $("#wrapper").hide();
                    $body.toggleClass("xm");
                    $body.append($lma);
                    $lma.animate({ opacity: 1 }, 3000, function () {
                        setTimeout(function () {
                            $(document).snowfall({
                                flakeCount: 250, maxSpeed: 10, image: function () {
                                    var rnd = parseInt(Math.random() * 1000 % 19) + 1;
                                    return "Custom/XM/images/flake-" + rnd + ".png";
                                }, minSize: 10, maxSize: 32
                            });
                            var elements = document.querySelectorAll(".snowfall-flakes");
                            var animationDuration = 50000;
                            document.querySelectorAll(".snowfall-flakes").forEach(function(element) {
                                var randomDuration = Math.floor(Math.random() * animationDuration);
                                element.style.animationDelay = randomDuration + "ms";
								element.style.animationDuration = (randomDuration % 100 + 4) + "s";
								element.style.animationName = "spin-" + (randomDuration % 7 + 1);
                            });
                        }, 1000);
                    });
                });
            }, 1 * 60 * 1000);
        }
    }
});